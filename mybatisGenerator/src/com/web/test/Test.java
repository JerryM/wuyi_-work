package com.web.test;

import com.web.bean.Students;
import com.web.bean.StudentsExample;
import com.web.mapper.StudentsMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class Test {

    public static void main(String[] args) throws IOException {
        //1、加载mybatis的全局配置文件
        InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
        //2、生成sqlsessionfactory【会话的工厂类】【用于生成session会话】
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(is);
        //3、通过sqlsessionfactory来得到生成sqlsession会话对象    相当于Connection
        SqlSession session = sqlSessionFactory.openSession();
        //4、通过会话对象得到mapper接口对象
        StudentsMapper studentMapper = session.getMapper(StudentsMapper.class);
        //
//        Students students = new Students(11, "wangping", "123456", "男", "计算机", "2班", 0.0, 0);
        //通过调用操作方法  insert[插入所有字段]   insertSelective【有选择的插入】
//        studentMapper.insertSelective(students);//有选择性的添加
//        studentMapper.updateByPrimaryKeySelective(students);//根据主键有选择性的修改
//        studentMapper.deleteByPrimaryKey(11);//根据主键删除
//        System.out.println(studentMapper.selectByPrimaryKey(10));//根据主键查询

        //example的使用
        //模板的使用--条件的添加
//        Students students = new Students();
//        students.setClassName("2班");
//        //example模板对象
//        StudentsExample example = new StudentsExample();
//        //使用模板的内部类来添加条件
//        example.createCriteria().andNameEqualTo("隔壁老王");
//        //根据模板来修改对象数据  根据name来修改班级信息
//        studentMapper.updateByExampleSelective(students, example);


        //删除性别为男并且学院为生活艺术的数据
//        StudentsExample example = new StudentsExample();
//        //使用模板的内部类来添加条件
//        example.createCriteria().andSexEqualTo("男").andCollegeEqualTo("生活艺术");
//        studentMapper.deleteByExample(example);

        //查询所有
//        StudentsExample example = new StudentsExample();
//        //没有添加条件，则是查询所有数据
//        List<Students> list = studentMapper.selectByExample(example);
//        for (Students stu : list) {
//            System.out.println(stu);
//        }

        //根据name进行模糊查询    like
        StudentsExample example = new StudentsExample();
        //使用模板的内部类来添加条件
        example.createCriteria().andNameLike("%n%");
        //没有添加条件，则是查询所有数据
        List<Students> list = studentMapper.selectByExample(example);
        for (Students stu : list) {
            System.out.println(stu);
        }

        //添加、删除、修改操作要提交事务
        session.commit();
    }


}
