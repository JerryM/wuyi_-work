package com.web.dao;


import com.web.bean.Business;
import com.web.bean.Dish;
import com.web.bean.PageModel;
import com.web.bean.Dish;
import com.web.jdbc.DataSourceTool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

//针对学生模块的数据库操作代码的封装类  【所有数据库操作方法】
public class DishDao {

    //测试代码
    public static void main(String[] args) {
        DishDao dao = new DishDao();
        List<Dish> list = dao.queryDish();
        for (Dish item : list) {
            System.out.println(item);
        }
    }

    //查询  :   返回结果集  --集合  [嵌套查询--调用另一条sql语句查询商家信息]
    public List<Dish> queryDish() {
        //创建一个集合用于保存数据
        List<Dish> list = new ArrayList<>();
        //--获取数据库链接
        Connection connection = DataSourceTool.getConnection();
        // -- 定义sql语句  占位符表示一个动态数据的入参
        String sql = "select d.*,b.name bus_name from business b,dish d where b.id=d.business_id;";
        try {
            //--通过链接对象获取sql语句对象
            PreparedStatement ps = connection.prepareStatement(sql);//提供sql语句
            //--通过sql语句对象执行sql操作
            ResultSet result = ps.executeQuery();
            //遍历结果集
            while (result.next()) {//判断是否有下一条记录
                //获取数据是通过getXXX方法来获取   【根据字段名   、    根据索引】
                Dish stu = new Dish();
                stu.setId(result.getInt("id"));
                stu.setName(result.getString("name"));
                stu.setImage(result.getString("image"));
                stu.setRemark(result.getString("remark"));
                stu.setMaterial(result.getString("material"));
                stu.setTypeId(result.getInt("type_id"));
                stu.setBusinessId(result.getInt("business_id"));
                //调用另一条sql查询商家的信息
                Business bus = BusinessDao.queryBusinessById(result.getInt("business_id"));
                stu.setBusinessName(bus.getName());
                //每得到一个对象则保存在集合中
                list.add(stu);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            //--关闭链接
            DataSourceTool.closeConnection(connection);
        }
        // 返回list
        return list;
    }

   /* //查询  :   返回结果集  --集合  [多表的联结查询]
    public List<Dish> queryDish() {
        //创建一个集合用于保存数据
        List<Dish> list = new ArrayList<>();
        //--获取数据库链接
        Connection connection = DataSourceTool.getConnection();
        // -- 定义sql语句  占位符表示一个动态数据的入参
        String sql = "select d.*,b.name bus_name from business b,dish d where b.id=d.business_id;";
        try {
            //--通过链接对象获取sql语句对象
            PreparedStatement ps = connection.prepareStatement(sql);//提供sql语句
            //--通过sql语句对象执行sql操作
            ResultSet result = ps.executeQuery();
            //遍历结果集
            while (result.next()) {//判断是否有下一条记录
                //获取数据是通过getXXX方法来获取   【根据字段名   、    根据索引】
                Dish stu = new Dish();
                stu.setId(result.getInt("id"));
                stu.setName(result.getString("name"));
                stu.setImage(result.getString("image"));
                stu.setRemark(result.getString("remark"));
                stu.setMaterial(result.getString("material"));
                stu.setTypeId(result.getInt("type_id"));
                stu.setBusinessId(result.getInt("business_id"));
                stu.setBusinessName(result.getString("bus_name"));
                //每得到一个对象则保存在集合中
                list.add(stu);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            //--关闭链接
            DataSourceTool.closeConnection(connection);
        }
        // 返回list
        return list;
    }*/


}
