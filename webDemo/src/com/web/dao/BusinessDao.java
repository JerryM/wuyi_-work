package com.web.dao;


import com.web.bean.PageModel;
import com.web.bean.Business;
import com.web.jdbc.DataSourceTool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

//针对学生模块的数据库操作代码的封装类  【所有数据库操作方法】
public class BusinessDao {


    //根据主键查询对象数据
    public static Business queryBusinessById(int bid) {
//        --获取数据库链接【通信】
        Connection connection = DataSourceTool.getConnection();
//        --定义操作的sql语句【占位符-动态入参数据】
        String sql = "select * from business where id=?";
        try {
//        --通过链接获取sql语句对象【sql】【预编译--占位符】
            PreparedStatement ps = connection.prepareStatement(sql);
//        --通过sql语句对象，给占位符赋值
            ps.setInt(1, bid);
//        --通过sql语句对象来执行sql语句【excuteUpdate\excuteQuery(ResultSet)】
            ResultSet result = ps.executeQuery();
            //获取结果  理解+赋值粘贴+修改===》理解+独立开发
            if (result.next()) {
                //获取数据是通过getXXX方法来获取   【根据字段名   、    根据索引】
                Business stu = new Business();
                stu.setId(result.getInt("id"));
                stu.setName(result.getString("name"));
                stu.setPassword(result.getString("password"));
                stu.setHostName(result.getString("host_name"));
                stu.setPhone(result.getString("phone"));
                stu.setAddress(result.getString("address"));
                stu.setContent(result.getString("content"));
                stu.setIncome(result.getDouble("income"));
                stu.setStatus(result.getInt("status"));
                //返回
                return stu;
            }
//        --关闭链接
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            //--关闭链接
            DataSourceTool.closeConnection(connection);
        }
        //返回null
        return null;
    }


}
