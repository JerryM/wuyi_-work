package com.web.dao;


import com.web.bean.PageModel;
import com.web.bean.Student;
import com.web.jdbc.DataSourceTool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

//针对学生模块的数据库操作代码的封装类  【所有数据库操作方法】
public class StudentDao {

    //删除
    public boolean deleteStudent(int id) {
        //--获取数据库链接
        Connection connection = DataSourceTool.getConnection();
        // -- 定义sql语句  占位符表示一个动态数据的入参
        String sql = "delete from students where id=?;";
        try {
            //--通过链接对象获取sql语句对象
            PreparedStatement ps = connection.prepareStatement(sql);//提供sql语句
            //--给sql语句对象赋值  赋值的个数和占位符的个数要一致  第一个参数为占位符的位置  第二个参数为入参的数值
            ps.setInt(1, id);
            //--通过sql语句对象执行sql操作
            int result = ps.executeUpdate();
            //判断影响行数，返回结果
            if (result > 0) {
                return true;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            //--关闭链接
            DataSourceTool.closeConnection(connection);
        }
        // 如果上面的操作没有返回true  则默认返回false
        return false;
    }

    //添加
    public boolean insertStudent(Student student) {
        //--获取数据库链接
        Connection connection = DataSourceTool.getConnection();
        // -- 定义sql语句  占位符表示一个动态数据的入参
        String sql = "insert into students(name,sex,password,college,class_name) values(?,?,?,?,?);";
        try {
            //--通过链接对象获取sql语句对象
            PreparedStatement ps = connection.prepareStatement(sql);//提供sql语句
            //--给sql语句对象赋值  赋值的个数和占位符的个数要一致  第一个参数为占位符的位置  第二个参数为入参的数值
            ps.setString(1, student.getName());
            ps.setString(2, student.getSex());
            ps.setString(3, student.getPassword());
            ps.setString(4, student.getCollege());
            ps.setString(5, student.getClassName());
            //--通过sql语句对象执行sql操作
            int result = ps.executeUpdate();
            //判断影响行数，返回结果
            if (result > 0) {
                return true;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            //--关闭链接
            DataSourceTool.closeConnection(connection);
        }
        // 如果上面的操作没有返回true  则默认返回false
        return false;
    }

    //修改   根据主键修改其他字段的数据
    public boolean updateStudent(Student student) {
        //--获取数据库链接
        Connection connection = DataSourceTool.getConnection();
        // -- 定义sql语句  占位符表示一个动态数据的入参
        String sql = "update students set name=?,sex=?,password=?,college=?,class_name=?,status=? where id = ?;";
        try {
            //--通过链接对象获取sql语句对象
            PreparedStatement ps = connection.prepareStatement(sql);//提供sql语句
            //--给sql语句对象赋值  赋值的个数和占位符的个数要一致  第一个参数为占位符的位置  第二个参数为入参的数值
            ps.setString(1, student.getName());
            ps.setString(2, student.getSex());
            ps.setString(3, student.getPassword());
            ps.setString(4, student.getCollege());
            ps.setString(5, student.getClassName());
            ps.setInt(6, student.getStatus());
            ps.setInt(7, student.getId());
            //--通过sql语句对象执行sql操作
            int result = ps.executeUpdate();
            //判断影响行数，返回结果
            if (result > 0) {
                return true;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            //--关闭链接
            DataSourceTool.closeConnection(connection);
        }
        // 如果上面的操作没有返回true  则默认返回false
        return false;
    }

    //根据主键查询对象数据
    public Student queryStudentById(int sid) {
//        --获取数据库链接【通信】
        Connection connection = DataSourceTool.getConnection();
//        --定义操作的sql语句【占位符-动态入参数据】
        String sql = "select * from students where id=?";
        try {
//        --通过链接获取sql语句对象【sql】【预编译--占位符】
            PreparedStatement ps = connection.prepareStatement(sql);
//        --通过sql语句对象，给占位符赋值
            ps.setInt(1, sid);
//        --通过sql语句对象来执行sql语句【excuteUpdate\excuteQuery(ResultSet)】
            ResultSet result = ps.executeQuery();
            //获取结果  理解+赋值粘贴+修改===》理解+独立开发
            if (result.next()) {
                //获取数据是通过getXXX方法来获取   【根据字段名   、    根据索引】
                Student stu = new Student();
                stu.setId(result.getInt("id"));
                stu.setName(result.getString("name"));
                stu.setSex(result.getString("sex"));
                stu.setPassword(result.getString("password"));
                stu.setCollege(result.getString("college"));
                stu.setClassName(result.getString("class_name"));
                stu.setMoney(result.getDouble("money"));
                stu.setStatus(result.getInt("status"));
                //返回
                return stu;
            }
//        --关闭链接
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            //--关闭链接
            DataSourceTool.closeConnection(connection);
        }
        //返回null
        return null;
    }

    //查询 总记录条数方法
    public int queryStudentCount(String search) {
        //--获取数据库链接
        Connection connection = DataSourceTool.getConnection();
        // -- 定义sql语句  占位符表示一个动态数据的入参
        String sql = "select count(*) from students where name like ?;";
        try {
            //--通过链接对象获取sql语句对象
            PreparedStatement ps = connection.prepareStatement(sql);//提供sql语句
            //
            ps.setString(1, "%"+search+"%");
            //--通过sql语句对象执行sql操作
            ResultSet result = ps.executeQuery();
            //遍历结果集
            if (result.next()) {//判断是否有下一条记录
                //返回结果
                return result.getInt(1);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            //--关闭链接
            DataSourceTool.closeConnection(connection);
        }
        // 返回0
        return 0;
    }

    //查询  :   返回结果集  --集合
    public List<Student> queryStudent(String search , PageModel pageModel) {
        //创建一个集合用于保存数据
        List<Student> list = new ArrayList<>();
        //--获取数据库链接
        Connection connection = DataSourceTool.getConnection();
        // -- 定义sql语句  占位符表示一个动态数据的入参
        String sql = "select * from students where name like ? limit ?,?;";
        try {
            //--通过链接对象获取sql语句对象
            PreparedStatement ps = connection.prepareStatement(sql);//提供sql语句
            //给占位符赋值
            ps.setString(1, "%"+search+"%");
            ps.setInt(2, pageModel.getStartIndex());
            ps.setInt(3, pageModel.getPageSize());
            //--通过sql语句对象执行sql操作
            ResultSet result = ps.executeQuery();
            //遍历结果集
            while (result.next()) {//判断是否有下一条记录
                //获取数据是通过getXXX方法来获取   【根据字段名   、    根据索引】
                Student stu = new Student();
                stu.setId(result.getInt("id"));
                stu.setName(result.getString("name"));
                stu.setSex(result.getString("sex"));
                stu.setPassword(result.getString("password"));
                stu.setCollege(result.getString("college"));
                stu.setClassName(result.getString("class_name"));
                stu.setMoney(result.getDouble("money"));
                stu.setStatus(result.getInt("status"));
                //每得到一个对象则保存在集合中
                list.add(stu);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            //--关闭链接
            DataSourceTool.closeConnection(connection);
        }
        // 返回list
        return list;
    }

    //登录查询  根据用户名及密码进行查询，有则返回对象，没有则返回null
    public Student queryStudent(String login_name, String login_password) {
        //--获取数据库链接
        Connection connection = DataSourceTool.getConnection();
        // -- 定义sql语句  占位符表示一个动态数据的入参
        String sql = "select * from students where name=? and password = ?;";
        try {
            //--通过链接对象获取sql语句对象
            PreparedStatement ps = connection.prepareStatement(sql);//提供sql语句
            //
            ps.setString(1, login_name);
            ps.setString(2, login_password);
            //--通过sql语句对象执行sql操作
            ResultSet result = ps.executeQuery();
            // 有则返回
            if (result.next()) {//判断是否有下一条记录
                //获取数据是通过getXXX方法来获取   【根据字段名   、    根据索引】
                Student stu = new Student();
                stu.setId(result.getInt("id"));
                stu.setName(result.getString("name"));
                stu.setSex(result.getString("sex"));
                stu.setPassword(result.getString("password"));
                stu.setCollege(result.getString("college"));
                stu.setClassName(result.getString("class_name"));
                stu.setMoney(result.getDouble("money"));
                stu.setStatus(result.getInt("status"));
                //每得到一个对象则保存在集合中
                return stu;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            //--关闭链接
            DataSourceTool.closeConnection(connection);
        }
        // 没有返回null
        return null;
    }
}
