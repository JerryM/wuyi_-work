package com.web.controller;

import com.web.bean.Student;
import com.web.dao.StudentDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

//定义当前请求处理的访问uri
@WebServlet("/toUpdateStudent")
public class StudentToUpdateController extends HttpServlet {

    //http请求默认都是get方式
    //doGet用于处理get请求方式
    @Override           //req请求对象                           resp响应对象
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //给请求设置编码：
        req.setCharacterEncoding("UTF-8");
        //1、从请求中获取提交的数据   【getParameter方法得到的都是字符串类型】
        String sid = req.getParameter("id");
        int id = Integer.parseInt(sid);
        //2、调用数据库的操作方法删除数据
        StudentDao dao = new StudentDao();
        Student student = dao.queryStudentById(id);
        //3、保存-跳转页面
        req.setAttribute("stu",student);
        req.getRequestDispatcher("/StudentUpdate.jsp").forward(req, resp);
    }

    //doPost用于处理post请求方式
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
