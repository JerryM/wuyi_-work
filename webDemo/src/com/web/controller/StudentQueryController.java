package com.web.controller;

import com.web.bean.PageModel;
import com.web.bean.Student;
import com.web.dao.StudentDao;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

//定义当前请求处理的访问uri
@WebServlet("/getAllStudent")
public class StudentQueryController extends HttpServlet {

    //http请求默认都是get方式
    //doGet用于处理get请求方式
    @Override           //req请求对象                           resp响应对象
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //给请求设置编码：
        req.setCharacterEncoding("UTF-8");
        //获取跳转的页码
        String pageNo = req.getParameter("pageNo");
        String search = req.getParameter("search");
        if (search == null) {
            search = "";
        }
        int pageNum = 1;
        if (pageNo != null && !pageNo.equals("")) {
            pageNum = Integer.parseInt(pageNo);
        }
        //1、调用数据库的操作方法查询集合数据
        StudentDao dao = new StudentDao();
        //构建分页对象                   访问的页码   总记录条数
        PageModel page = new PageModel(pageNum, dao.queryStudentCount(search));
        List<Student> list = dao.queryStudent(search, page);
        //2、将集合数据保存在作用域中【容器】
        req.setAttribute("list", list);
        req.setAttribute("pageInfo", page);
        req.setAttribute("search", search);
        //3、跳转页面
        req.getRequestDispatcher("/StudentIndex.jsp").forward(req, resp);
    }

    //doPost用于处理post请求方式
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
