package com.web.controller;

import com.web.bean.Student;
import com.web.dao.StudentDao;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

//定义当前请求处理的访问uri
@WebServlet("/login")
public class StudentLoginController extends HttpServlet {
    //http://localhost:8080/应用的访问uri/请求处理类的uri?参数列表
    //http://localhost:8080/webDemo/login?username=admin&password=123

    //http请求默认都是get方式
    //doGet用于处理get请求方式
    @Override           //req请求对象                           resp响应对象
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //给请求设置编码：
        req.setCharacterEncoding("UTF-8");
        //请求处理操作：
        //1、获取请求提交的数据  通过getParameter方法根据key来获取value
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        //判断用户名为空
        if(username==null||username.equals("")){
            req.setAttribute("mess","用户名不能为空");
            //通过请求分发器指定跳转路径，然后进行跳转
            req.getRequestDispatcher("/login.jsp").forward(req,resp);
            //结束方法
            return;
        }
        //调用数据库的操作方法来查询用户数据
        StudentDao dao = new StudentDao();
        Student student = dao.queryStudent(username,password);
        //2、进行逻辑校验【判断是否登录成功】   admin -- 123
        if (student!=null) {//对象数据不为空表示登录成功
            //3、通过响应对象作出响应，在页面输出字符串数据
            //resp.getWriter().append("login success!!");
            //页面跳转：  【转发   、  重定向】
            // -- 获取请求分发器【指定跳转的路径】
            RequestDispatcher requestDispatcher = req.getRequestDispatcher("/index.html");
            // -- 通过请求分发器，向前一步跳转
            requestDispatcher.forward(req,resp);
        } else {
            //3、作出响应
            //resp.getWriter().append("login failed!!");
            //保存错误信息到容器中【作用域】  请求对象是请求作用域
            req.setAttribute("mess","用户名或密码错误");
            //通过请求分发器指定跳转路径，然后进行跳转
            req.getRequestDispatcher("/login.jsp").forward(req,resp);
        }
    }

    //doPost用于处理post请求方式
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
