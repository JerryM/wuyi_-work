package com.web.controller;

import com.web.bean.Student;
import com.web.dao.StudentDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

//定义当前请求处理的访问uri
@WebServlet("/insertStudent")
public class StudentInsertController extends HttpServlet {

    //http请求默认都是get方式
    //doGet用于处理get请求方式
    @Override           //req请求对象                           resp响应对象
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //给请求设置编码：
        req.setCharacterEncoding("UTF-8");
        //1、从请求中获取提交的数据、封装对象
        String name = req.getParameter("name");
        String sex = req.getParameter("sex");
        String password = req.getParameter("password");
        String college = req.getParameter("college");
        String className = req.getParameter("className");
        //封装对象
        Student stu = new Student(0, name, sex, password, college, className, 0, 0);
        //2、调用数据库的操作方法添加数据
        StudentDao dao = new StudentDao();
        boolean isok = dao.insertStudent(stu);
        //3、响应-跳转页面
        if (isok) {//添加成功--重新查询列表--发起查询请求
            req.getRequestDispatcher("/getAllStudent").forward(req, resp);
        } else {//失败--重新跳转到添加页面
            //保存错误信息
            req.setAttribute("message", "添加操作失败，请重新操作");
            req.setAttribute("stu", stu);//保存对象数据，在页面表单中回显
            //
            req.getRequestDispatcher("/StudentInsert.jsp").forward(req, resp);
        }
    }

    //doPost用于处理post请求方式
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
