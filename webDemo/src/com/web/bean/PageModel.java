package com.web.bean;

//分页的数据模型
public class PageModel {
    // 每页显示数据记录条数
    private int pageSize = 2;
    // 当前页码
    private int pageNum;
    // 总页数
    private int pages;
    // 总记录条数
    private int total;
    // 上一页
    private int prePage;
    // 下一页
    private int nextPage;
    // sql语句查询起始位置
    private int startIndex;

    // 0 2 4
    // 第一页 每页显示2条数据 limit 0,2
    // 第二页 每页显示2条数据 limit 2,2
    // 第三页 每页显示2条数据 limit 4,2
    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    //总页数       总记录数/每页显示的记录条数 (7+3-1)/3=3
    public int getPages() {
        return (total + pageSize - 1) / pageSize;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public int getTotal() {
        return total;
    }

    //总记录数
    public void setTotal(int total) {
        this.total = total;
    }

    //上一页
    public int getPrePage() {
        int pre = this.pageNum - 1;
        if (pre <= 0) {
            return 1;
        }
        return pre;
    }

    public void setPrePage(int prePage) {
        this.prePage = prePage;
    }

    //下一页
    public int getNextPage() {
        int next = this.pageNum + 1;
        //如果next超过总页数，则赋值为总页数
        if (next > this.getPages()) {
            return this.getPages();
        }
        return next;
    }

    public void setNextPage(int nextPage) {
        this.nextPage = nextPage;
    }

    //起始位置      0   3   6    (2-1)*3
    public int getStartIndex() {
        int item = (pageNum - 1) * pageSize;
        return item;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    //构造器        跳转当前页码、总记录数
    public PageModel(int pageNum, int total) {
        // 合理化操作
        this.total = total;
        // 如果输入的页码是大于总页数，就赋值为总页数
        if (pageNum > this.getPages()) {
            this.pageNum = this.getPages();
        } // 如果输入的页码是小于总页数，就赋值为1
        else if (pageNum <= 0) {
            this.pageNum = 1;
        } // 合理的情况
        else {
            this.pageNum = pageNum;
        }
    }

}
