package com.web.service;

import com.web.bean.Students;
import com.web.bean.StudentsExample;
import com.web.mapper.StudentsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

//表明当前类为业务层的代码、交给ioc容器管理
@Service
public class StudentsServiceImpl implements StudentsService {

    //依赖注入
    @Autowired
    StudentsMapper studentsMapper;

    //登录的业务方法---实现逻辑
    @Override
    public Students login(String name, String password) {
        //创建模板对象
        StudentsExample example = new StudentsExample();
        //添加查询条件
        example.createCriteria().andNameEqualTo(name).andPasswordEqualTo(password);
        //查询操作
        List<Students> list = studentsMapper.selectByExample(example);
        //返回结果
        return list.size() > 0 ? list.get(0) : null;
    }

    //分页+模糊查询---实现逻辑
    @Override
    public List<Students> queryStudent(String key) {
        //创建模板对象
        StudentsExample example = new StudentsExample();
        //添加查询条件
        example.createCriteria().andNameLike("%" + key + "%");
        //查询操作
        List<Students> list = studentsMapper.selectByExample(example);
        //返回结果
        return list;
    }
}
