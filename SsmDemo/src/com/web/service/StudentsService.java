package com.web.service;

import com.web.bean.Students;

import java.util.List;

//用于定义规范【抽象方法】
public interface StudentsService {

    //登录方法
    public Students login(String name, String password);

    //分页模糊查询方法
    public List<Students> queryStudent(String key);

}
