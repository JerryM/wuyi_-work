package com.web.bean;

public class Students {
    private Integer id;

    private String name;

    private String password;

    private String sex;

    private String college;

    private String className;

    private Double money;

    private Integer status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex == null ? null : sex.trim();
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college == null ? null : college.trim();
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className == null ? null : className.trim();
    }

    public Double getMoney() {
        return money;
    }

    public void setMoney(Double money) {
        this.money = money;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Students() {
    }

    @Override
    public String toString() {
        return "Students{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", sex='" + sex + '\'' +
                ", college='" + college + '\'' +
                ", className='" + className + '\'' +
                ", money=" + money +
                ", status=" + status +
                '}';
    }

    public Students(Integer id, String name, String password, String sex, String college, String className, Double money, Integer status) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.sex = sex;
        this.college = college;
        this.className = className;
        this.money = money;
        this.status = status;
    }
}