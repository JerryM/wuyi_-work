package com.web.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.web.bean.Students;
import com.web.service.StudentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class StudentsController {

    @Autowired
    StudentsService service;

    //登录的请求处理方法
    @RequestMapping("/login")
    public String login(String username, String password, HttpServletRequest request) {
        //1、调用业务层的方法来执行操作
        Students login = service.login(username, password);
        //2、作出响应
        if (login != null) {
            return "redirect:/getAllStudent";
        } else {
            //保存错误信息到容器中【作用域】  请求对象是请求作用域
            request.setAttribute("mess", "用户名或密码错误");
            return "login";
        }
    }

    //查询的请求处理方法
    @RequestMapping("/getAllStudent")
    public String getAllStudent(@RequestParam(required = false, defaultValue = "") String search,
                                @RequestParam(required = false, defaultValue = "1") int pageNo,
                                HttpServletRequest request) {
        //1、查询方法来执行操作
        //开启分页查询，提供页码及显示条数
        PageHelper.startPage(pageNo, 3);
        List<Students> list = service.queryStudent(search);
        //获取分页对象
        PageInfo<Students> pageInfo = new PageInfo<>(list);
        //2、保存数据
        request.setAttribute("list", list);
        //分页对象
        request.setAttribute("pageInfo", pageInfo);
        request.setAttribute("search", search);
        //3、跳转页面
        return "StudentIndex";
    }

}
