<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 2022/3/28
  Time: 9:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="side-menu">
    <div class='side-menu-body'>
        <ul>
            <li class="side-menu-divider">Navigation</li>
            <li>
                <a href="#"><i class="icon ti-home"></i> <span>学生模块</span> </a>
                <ul>
                    <li><a href="/webDemo/getAllStudent">学生列表</a></li>
                    <li><a href="StudentInsert.jsp">添加学生</a></li>
                </ul>
            </li>
            <li>
                <a data-attr="layout-builder-toggle" href="#">
                    <i class="icon ti-layout"></i>
                    <span>订单模块</span>
                </a>
                <ul>
                    <li><a href="layout-container.html">Container</a></li>
                    <li><a href="layout-dark.html">Dark</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- end::side menu -->
