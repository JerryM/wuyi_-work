package com.web.controller;

import com.web.bean.Student;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

//controller代码   标志注解    方法级别  【简化操作】【本质上就是封装的servlet】
@Controller
public class StudentController {

    //登录请求定义
    @RequestMapping("/userlogin")
    @ResponseBody  //不返回视图，输出数据
    public String login(String name, String password) {
        if (name.equals("admin") && password.equals("123")) {
            return "success";
        } else {
            return "failed";
        }
    }


    //添加请求定义
    @RequestMapping("/insertUser")
    //@ResponseBody  //不返回视图，输出数据
    public String insertUser(Student stu, HttpServletRequest request) {
        //
        System.out.println(stu);
        //
        if (stu.getName().equals("admin")) {
            return "redirect:/index.html";//   重定向 到/index.html
            //return "index";//   /StudentIndex.jsp  index.html
        } else {
            //保存数据到作用域中
            request.setAttribute("stu", stu);
            //通过视图解析器进行跳转
            return "StudentInsert";//转发   /StudentInsert.jsp
        }
    }

}
