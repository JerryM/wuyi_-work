package com.web.mapper;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

public class Test {

    public static void main(String[] args) throws IOException {
        //1、加载mybatis的全局配置文件
        InputStream is = Resources.getResourceAsStream("mybatis-config.xml");
        //2、生成sqlsessionfactory【会话的工厂类】【用于生成session会话】
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(is);
        //3、通过sqlsessionfactory来得到生成sqlsession会话对象    相当于Connection
        SqlSession session = sqlSessionFactory.openSession();
        //4、通过会话对象得到mapper接口对象
        StudentMapper studentMapper = session.getMapper(StudentMapper.class);
        //通过调用操作方法
        studentMapper.insertStudent();
        //添加、删除、修改操作要提交事务
        session.commit();
    }


}
