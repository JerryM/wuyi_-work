package com.web.mapper;

//mapper接口文件用于绑定mapper.xml映射文件
public interface StudentMapper {

    //方法名要和sql语句id要一致
    public int insertStudent();

}
