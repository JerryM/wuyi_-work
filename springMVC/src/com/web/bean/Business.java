package com.web.bean;

import java.io.Serializable;

/**
 * 商家
 *
 * @author Jerry
 * @email Jerry@gmail.com
 * @date 2022-03-01 22:20:52
 */
public class Business implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    private Integer id;
    /**
     *
     */
    private String name;
    /**
     *
     */
    private String password;
    /**
     *
     */
    private String hostName;
    /**
     *
     */
    private String phone;
    /**
     *
     */
    private String address;
    /**
     *
     */
    private String content;
    /**
     *
     */
    private Double income;

    @Override
    public String toString() {
        return "Business{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", hostName='" + hostName + '\'' +
                ", phone='" + phone + '\'' +
                ", address='" + address + '\'' +
                ", content='" + content + '\'' +
                ", income=" + income +
                ", status=" + status +
                '}';
    }

    /**
     *
     */
    private Integer status;

    public Business() {
    }

    public Business(Integer id, String name, String password, String hostName, String phone, String address, String content, Double income, Integer status) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.hostName = hostName;
        this.phone = phone;
        this.address = address;
        this.content = content;
        this.income = income;
        this.status = status;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Double getIncome() {
        return income;
    }

    public void setIncome(Double income) {
        this.income = income;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
