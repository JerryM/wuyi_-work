package com.web.bean;

import java.io.Serializable;

/**
 * 菜品
 *
 * @author Jerry
 * @email Jerry@gmail.com
 * @date 2022-03-01 22:20:52
 */
public class Dish implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    private Integer id;
    /**
     *
     */
    private String name;
    /**
     *
     */
    private String image;
    /**
     *
     */
    private String remark;
    /**
     *
     */
    private String material;
    /**
     *
     */
    private Integer typeId;
    /**
     *
     */
    private String typeName;
    /**
     *
     */
    private Integer businessId;
    /**
     * 字段的管理 【一对一商家】
     */
    private String businessName;

    @Override
    public String toString() {
        return "Dish{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", image='" + image + '\'' +
                ", remark='" + remark + '\'' +
                ", material='" + material + '\'' +
                ", typeId=" + typeId +
                ", typeName='" + typeName + '\'' +
                ", businessId=" + businessId +
                ", businessName='" + businessName + '\'' +
                '}';
    }

    public Dish() {
    }

    public Dish(Integer id, String name, String image, String remark, String material, Integer typeId, String typeName, Integer businessId, String businessName) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.remark = remark;
        this.material = material;
        this.typeId = typeId;
        this.typeName = typeName;
        this.businessId = businessId;
        this.businessName = businessName;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public Integer getBusinessId() {
        return businessId;
    }

    public void setBusinessId(Integer businessId) {
        this.businessId = businessId;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }
}
