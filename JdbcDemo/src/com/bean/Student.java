package com.bean;

//学生的实体类  -- 对应students的数据库表
public class Student {
    // 定义私有化属性  lombook
    private int id;
    private String name;
    private String sex;
    private String password;
    private String college;
    private String className;
    private double money;
    private int status;

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                ", password='" + password + '\'' +
                ", college='" + college + '\'' +
                ", className='" + className + '\'' +
                ", money=" + money +
                ", status=" + status +
                '}';
    }

    public Student() {
    }

    public Student(int id, String name, String sex, String password, String college, String className, double money, int status) {
        this.id = id;
        this.name = name;
        this.sex = sex;
        this.password = password;
        this.college = college;
        this.className = className;
        this.money = money;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
