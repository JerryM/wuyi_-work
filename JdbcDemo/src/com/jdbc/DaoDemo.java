package com.jdbc;

import com.bean.Student;
import com.dao.StudentDao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

//用于链接数据库操作
public class DaoDemo {

    //实现查询操作
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        //通过工具类来获取数据库链接对象【会话】
        StudentDao dao = new StudentDao();

        // 创建学生对象
        // Student student = new Student(0, "admin", "女", "123", "计算机", "计算机3班", 0, 0);
        // 调用dao的添加方法来添加记录
        // boolean isok = dao.insertStudent(student);
        // System.out.println(isok);

        //查询
        List<Student> list =  dao.queryStudent();
        for (Student stu:list){
            System.out.println(stu);
        }

        System.out.println("登录操作：");
        System.out.println(dao.queryStudent("admin","123456"));

    }

}
