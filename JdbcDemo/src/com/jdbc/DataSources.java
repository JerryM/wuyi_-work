package com.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

//用于链接数据库操作
public class DataSources {

    //    public static void main(String[] args) throws ClassNotFoundException, SQLException {
//        //1、定义链接数据库的信息
//        String name = "root";
//        String password = "";
//        String url = "jdbc:mysql://localhost:3306/dishorder?useUnicode=true&useSSL=false&characterEncoding=utf8";
//        //2、 加载数据库的驱动
//        Class.forName("com.mysql.jdbc.Driver");
//        //3、通过驱动管理者来获取数据库链接对象【会话】
//        Connection connection = DriverManager.getConnection(url,name,password);
//        //输出
//        System.out.println("获取到数据库链接对象："+connection);
//    }

//    public static void main(String[] args) throws ClassNotFoundException, SQLException {
//        //通过工具类来获取数据库链接对象【会话】
//        Connection connection = DataSourceTool.getConnection();
//        //输出
//        System.out.println("获取到数据库链接对象：" + connection);
//
//        //。。。数据操作代码。。。【添加学生的记录操作】
//        //insert into students(name,sex,password,college,class_name) values('zhang','男','123','计算机','20计算机1班');
//        //定义要执行的刷起来语句
//        String sql = "insert into students(name,sex,password,college,class_name) values('li','男','123','计算机','20计算机2班');";
//
//        // 根据主键来修改某条记录的数据
//        // String sql = "update students set name='ling',sex='男',password='123456',college='计算机',class_name='20计算机2班' where id = 2;";
//
//        // 根据主键来删除某条记录的数据
//        // String sql = "delete from students where id = 2;";
//        // 1、 通过链接来获取sql语句对象     【普通的sql语句对象    、    预编译的sql语句对象（安全、使用占位符）】
//        PreparedStatement ps = connection.prepareStatement(sql);//提供sql语句
//        // 2、 通过获取sql语句对象来执行sql    执行sql操作后的影响行数
//        int result = ps.executeUpdate();//添加、删除、修改可以使用executeUpdate方法
//        //
//        System.out.println("result=" + result);
//
//        //关闭链接
//        DataSourceTool.closeConnection(connection);
//    }

    //实现查询操作
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        //通过工具类来获取数据库链接对象【会话】
        Connection connection = DataSourceTool.getConnection();
        //输出
        System.out.println("获取到数据库链接对象：" + connection);

        //。。。数据操作代码。。。【添加学生的记录操作】
        // select * from students;
        //定义要执行的刷起来语句
        String sql = "select * from students;";
        // 1、 通过链接来获取sql语句对象     【普通的sql语句对象    、    预编译的sql语句对象（安全、使用占位符）】
        PreparedStatement ps = connection.prepareStatement(sql);//提供sql语句
        // 2、 通过获取sql语句对象来执行sql    执行sql操作后的得到结果集
        ResultSet result = ps.executeQuery();//查询使用eexecuteQuery方法
        //  遍历结果集，获取每一行的记录
        while (result.next()) {//判断是否有下一条记录
            //获取数据是通过getXXX方法来获取   【根据字段名   、    根据索引】
            System.out.print(result.getInt("id"));
            System.out.print(result.getString("name"));
            System.out.print(result.getString("sex"));
            System.out.print(result.getString("password"));
            System.out.print(result.getString("college"));
            System.out.print(result.getString("class_name"));
            System.out.print(result.getDouble("money"));
            System.out.print(result.getInt("status"));
            System.out.println();
        }
        //关闭链接
        DataSourceTool.closeConnection(connection);

    }

}
