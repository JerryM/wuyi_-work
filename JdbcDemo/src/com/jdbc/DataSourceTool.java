package com.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

//定义一个工具类用于获取数据库的链接对象
public class DataSourceTool {
    // 提供url、用户名、密码
    static String url = "jdbc:mysql://localhost:3306/dishorder?useUnicode=true&useSSL=false&characterEncoding=utf8";
    static String user = "root";
    static String password = "";

    //定义一个变量用于存放链接
    static Connection con = null;

    static {
        // 1、加载驱动类
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    //定义一个静态方法用于获取数据库的链接对象
    public static Connection getConnection() {
        // 通过驱动管理者来获取数据库的链接对象
        try {
            con = DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //返回数据库链接对象
        return con;
    }

    //定义一个静态方法用于关闭数据库的链接对象
    public static void closeConnection(Connection connection) {
        // 关闭链接
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
